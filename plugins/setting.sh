#!/usr/bin/env bash

plugin_category() {
  echo "Setup"
}

plugin_name() {
  echo "Mouse setup"
}

plugin_start() {
  sleep 3
}

plugin_message() {
  echo "this setting will change A => B"
}
