#!/usr/bin/env bash

plugin_category() {
  echo "Install"
}

plugin_name() {
  echo "Install Homebrew"
}

plugin_start() {
  export EXCEPTION="only for macbook"
  return 1
}

plugin_message() {
  echo "this will install homebrew"
}
