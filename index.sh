#!/usr/bin/env bash
# shellcheck disable=SC1000,SC1090

# generate by create-script-file v4.0.0
# link (https://github.com/Template-generator/create-script-file/tree/v4.0.0)

# set -x #DEBUG - Display commands and their arguments as they are executed.
# set -v #VERBOSE - Display shell input lines as they are read.
# set -n #EVALUATE - Check syntax of the script but don't execute.

#/ -----------------------------------
#/ Description:  ...
#/ How to:       ...
#/               ...
#/ Option:       --help | -h | -? | help | h | ?
#/                   > show this message
#/               --version | -v | version | v
#/                   > show command version
#/ -----------------------------------
#/ Create by:    Kamontat Chantrachirathunrong <kamontat.c@hotmail.com>
#/ Since:        05/02/2019
#/ -----------------------------------
#/ Error code    1      -- error
#/ -----------------------------------
#/ Known bug:    ...
#/ -----------------------------------
#// Version:      0.0.1   -- description
#//               0.0.2b1 -- beta-format
#//               0.0.2a1 -- alpha-format

_dirname="$(dirname "$0")"

source "${_dirname}/init-variable.sh"
source "${VAR_FILE}"
source "${PG_FILE}"

_message=""

pg_start
for file in "${PLUGINS_LIST[@]}"; do
  source "$file"

  cate="$(plugin_category)"
  name="$(plugin_name)"
  mess="$(plugin_message)"

  pg_mark "${cate}" "${name}"

  plugin_start || pg_show_error "${EXCEPTION}"

  _message="${_message}
## ${name} (${cate})
${mess}
"
done

pg_stop

# shellcheck disable=SC2059
printf "${_message}"
