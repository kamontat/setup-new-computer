#!/usr/bin/env bash
# shellcheck disable=SC1000

# generate by create-script-file v4.0.0
# link (https://github.com/Template-generator/create-script-file/tree/v4.0.0)

# set -x #DEBUG - Display commands and their arguments as they are executed.
# set -v #VERBOSE - Display shell input lines as they are read.
# set -n #EVALUATE - Check syntax of the script but don't execute.

#/ -----------------------------------
#/ Description:  ...
#/ How to:       ...
#/               ...
#/ Option:       --help | -h | -? | help | h | ?
#/                   > show this message
#/               --version | -v | version | v
#/                   > show command version
#/ -----------------------------------
#/ Create by:    Kamontat Chantrachirathunrong <kamontat.c@hotmail.com>
#/ Since:        05/02/2019
#/ -----------------------------------
#/ Error code    1      -- error
#/ -----------------------------------
#/ Known bug:    ...
#/ -----------------------------------
#// Version:      0.0.1   -- description
#//               0.0.2b1 -- beta-format
#//               0.0.2a1 -- alpha-format

# shellcheck disable=SC1090,SC2148

# Theme setting
export PG_RED="\033[1;31m"
export PG_GREEN="\033[1;32m"
export PG_YELLOW="\033[1;33m"
export PG_RESET="\033[0m"

# Progress setting
export PG_STYLE="shark"

export MESSAGE_LENGTH=35
export PG_PROCESS_COUNT=1

sec() {
  if command -v "gdate" &>/dev/null; then
    gdate +%s%3N
  else
    echo "0"
  fi
}

conv_time() {
  ! $PG_FORMAT_TIME && echo "$1" && return 0

  local ms="$1"
  printf '%02dm:%02ds:%03dms' $((ms / 60000)) $((ms % 60000 / 1000)) $((ms % 1000))
}

format_message() {
  local title="$1"
  shift
  local message="$*"
  printf "(%-10s) %s" "$title" "$message"
}

_message() {
  local color symbol raw_time dur message error

  color="$1"
  symbol="$2"
  raw_time="$3"
  message="$4"
  error="$5"

  dur="$(conv_time "${raw_time}")"

  if [[ "$error" != "" ]]; then
    error="-- ${color}Error: $error${PG_RESET}"
  fi

  printf "${color}[%s]${PG_RESET} %-${MESSAGE_LENGTH}s done in ${PG_YELLOW}%s${PG_RESET}. ${error}" "$symbol" "$message" "$dur"
}

show_message_by() {

  if $1; then
    completed_message "$2" "$3"
  else
    failure_message "$2" "$3" "$4"
  fi
  echo
}

completed_message() {
  local dur="$1"
  shift
  local message="$*"

  _message "$PG_GREEN" "+" "$dur" "$message"
}

failure_message() {
  local dur="$1"
  local message="$2"
  local error="$3"
  _message "$PG_RED" "-" "$dur" "$message" "$error"
}

export PG_START_TIME
PG_START_TIME="$(sec)"

export PG_PREV_TIME
PG_PREV_TIME="$(sec)"

export PG_PREV_MSG
PG_PREV_MSG=$(format_message "Start" "Initialization")

export PG_ERROR_MSG
PG_ERROR_MSG=""

export PG_PREV_STATE # true=success, false=failure
PG_PREV_STATE=true

pg_start() {
  "${ROOT}/revolver" -s "$PG_STYLE" start "${PG_GREEN}Initialization. Please Wait..."
}

pg_mark() {
  TIME=$(($(sec) - PG_PREV_TIME))

  "${ROOT}/revolver" -s "$PG_STYLE" update "${PG_GREEN}$2.."

  if "$PG_SHOW_PERF_INFO"; then
    show_message_by "$PG_PREV_STATE" "$TIME" "$PG_PREV_MSG" "$PG_ERROR_MSG"
  fi

  PG_PREV_TIME=$(sec)
  PG_PREV_MSG=$(format_message "$@")
  PG_PREV_STATE=true
  ((PG_PROCESS_COUNT++))
}

pg_show_error() {
  PG_PREV_STATE=false
  PG_ERROR_MSG="$1"
}

pg_stop() {
  TIME=$(($(sec) - PG_PREV_TIME))

  if "$PG_SHOW_PERF_INFO"; then
    show_message_by "$PG_PREV_STATE" "$TIME" "$PG_PREV_MSG" "$PG_ERROR_MSG"
  fi

  "${ROOT}/revolver" stop

  TIME=$(($(sec) - PG_START_TIME))

  printf "${PG_GREEN}[+]${PG_RESET} %-${MESSAGE_LENGTH}s      in ${PG_YELLOW}%s${PG_RESET}." "$(format_message "Completed" "Initialization $PG_PROCESS_COUNT tasks")" "$(conv_time "${TIME}")"
  echo
}
